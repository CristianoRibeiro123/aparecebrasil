<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aparece Brasil</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="images/logo.png" alt="" class="img-responsive">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-estado" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav  pull-right">
                    <li>
                        <a href="#">
                            <img src="images/estados/ac.jpg" alt="">
                            <br> AC
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/al.jpg" alt="">
                            <br> AL
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/ap.jpg" alt="">
                            <br> AP
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/am.jpg" alt="">
                            <br> AM
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/ba.jpg" alt="">
                            <br> BA
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/ce.jpg" alt="">
                            <br> CE
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/df.jpg" alt="">
                            <br> DF
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/es.jpg" alt="">
                            <br> ES
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/go.jpg" alt="">
                            <br> GO
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/ma.jpg" alt="">
                            <br> MA
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/mg.jpg" alt="">
                            <br> MG
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/ms.jpg" alt="">
                            <br> MS
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/mt.jpg" alt="">
                            <br> MT
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/pa.jpg" alt="">
                            <br> PA
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/pb.jpg" alt="">
                            <br> PB
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/pr.jpg" alt="">
                            <br> PR
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/pe.jpg" alt="">
                            <br> PE
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/pi.jpg" alt="">
                            <br> PI
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/rj.jpg" alt="">
                            <br> RJ
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/rn.jpg" alt="">
                            <br> RN
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/rs.jpg" alt="">
                            <br> RS
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/ro.jpg" alt="">
                            <br> RO
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/rr.jpg" alt="">
                            <br> RR
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/sc.jpg" alt="">
                            <br> SC
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/sp.jpg" alt="">
                            <br> SP
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/se.jpg" alt="">
                            <br> SE
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="images/estados/to.jpg" alt="">
                            <br> TO
                        </a>
                    </li>
                </ul>


                 <div class="col-md-6 col-sm-12 pull-right margin-top-10">
                     <form>
                        
                            <span class="col-md-2 col-sm-2 font-green">Cidades:</span> 
                       
                      
                            <select class="col-md-8 col-sm-2 form-control" name="" id="">
                                 <option value="01">Primeiro Selecione o Estado</option>
                            </select>
                        
                         
                        
                            <button type="submit" class="button-search btn"> Pesquisar</button>
                       
                         
                     </form>
                 </div>         

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<section class="first">
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7">
               <h1 class="titleBlack">
                   Tudo que você precisa saber 
               </h1>
               <h1 class="titleLight">
                   sobre a política da sua região
               </h1>
               <p class="paragrafo">
                   Acompanhe a rotina do seu parlamentar, interaja e saiba quais são as decisões que estão sendo
tomadas para o desenvolvimento do seu município, cidade, estado e país!
               </p>
            </div>

            <div class="col-md-4 col-sm-5">
                    <div class="col-md-6 link-comece">
                        <a href="#">Comece agora</a>
                    </div> 
                        
                    <div class="col-md-12 col-sm-12 col-xs-12 hr-comece"> 
                            <div class="col-md-3 col-xs-6 pull-right bg-hr"> </div>
                    </div>
                <ul id="list">
                    <li id="perfil">
                        <a href="#">
                            Perfil do Político
                        </a>
                    </li>
                    <li id="adm">
                        <a href="#">
                           Administralção Pública 
                        </a>
                    </li>
                    <li id="noticia">
                        <a href="#">
                            Notícias
                        </a>
                    </li>
                    <li id="eleicao">
                        <a href="#">
                            Eleições 2018
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <!-- /.container -->
</section>   

<section class="second">
    <div class="container">
        <h1>acesse</h1>
        <p>Crie seu perfil e tenha acesso a todas as funcionalidades</p>
        <button class="btn btn-login" data-toggle="modal" data-target="#myModal">Fazer Login</button>
    </div>
</section> 

<section class="third">
    <div class="container">
        <div class="col-md-6">
            <h4>O que é a Aparece Brasil</h4>
            <p>
                A Aparece Brasil surge no cenário nacional de tecnologia e comunicação para o meio político como uma proposta que visa aumentar a transparência e dinamismo no relacionamento entre governantes, parlamentares e população.
            </p>
            <br>
            <h4>Nossas Soluções</h4>
            <br>
             <img src="images/politico.png" alt="" class="img-responsive">
            <img src="images/campanha.png" alt="" class="img-responsive">
        </div>
        <div class="col-md-6">
            <h4>Fale Conosco</h4>
            <p>
                (61) 9999-9999 / (61) 9888-8888 <br>
                contato@aparecebrasil.com.br
            </p>
            
             @if (Session::get('success'))
                    <p class="alert alert-success"><b> {{ Session::get('success') }} </b></p>
             @endif

            <form  action="{!! url('/send') !!}" id="contact" method="POST" class="form" role="form">
                    {!! csrf_field() !!}
                    <div class="row">
                    <div class="col-xs-12 col-md-6 form-group">
                    <input class="form-control input-lg" id="name" name="nome" placeholder="Name" type="text" required autofocus  required />
                    </div>
                    <div class="col-xs-12 col-md-6 form-group">
                    <input class="form-control input-lg" id="email" name="email" placeholder="Email" type="email" required />
                    </div>
                    </div>
                    <textarea class="form-control input-lg" id="message" name="mensagem" placeholder="Message" rows="5" required></textarea>
                    <br />
                    <div class="row">
                    <div class="col-xs-12 col-md-12 form-group">
                    <button class="btn pull-right btn-send" type="submit">Enviar</button>
                </form>
            
        </div>
    </div>
</section>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body">
        
            <form class="form-horizontal" method="post">
           

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="textinput_username">Usuario</label>  
              <div class="col-md-9">
              <input id="username" name="textinput_username" type="text" placeholder="Username" class="form-control input-md" required="">
                
              </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
              <label class="col-md-3 control-label" for="passwordinput_password">Password</label>
              <div class="col-md-9">
                <input id="password" name="passwordinput_password" type="password" placeholder="Password" class="form-control input-md" required="">
                
              </div>
            </div>

            <!-- Button -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="singlebutton_login"></label>
              <div class="col-md-3">
                <button id="singlebutton_login" name="singlebutton_login" class="btn btn-primary">Login</button>
              </div>
            </div>

            
            </form>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
